This application is designed to do two things (nothing more):

1: Print out meta information about a cassandra cluster.  Keyspace name(s), table names, column names/types.
2: Optionally allow you to pass in additional information and have the script build and write out EasyCassandra stub beans based on meta info.
	- https://github.com/otaviojava/Easy-Cassandra

It downloads requirements through groovy grapes on first run. So make sure you have the latest groovy stable installed and set as the default.

then run:
groovy EZCassandraHelper.groovy

it will list all the options.