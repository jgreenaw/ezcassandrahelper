CREATE KEYSPACE ez_cassandra_helper WITH REPLICATION = {'class' : 'SimpleStrategy', 'replication_factor': 3};

CREATE TABLE ez_cassandra_helper.sample_tbl (col_key varchar, col_ascii ascii, col_bigint bigint, col_blob blob, col_boolean boolean, 
	col_decimal decimal, col_double double, col_float float, col_inet inet, 
	col_int int, col_list list<text>, col_map map<text,text>, col_set set<text>, col_text text, col_timestamp timestamp, 
	col_uuid uuid, col_timeuuid timeuuid, col_varchar varchar, col_varint varint, PRIMARY KEY (col_key));


