
@Grapes([
	@Grab(group='com.datastax.cassandra', module='cassandra-driver-core', version='2.0.0-beta1'),
	@Grab(group='org.xerial.snappy', module='snappy-java', version='1.1.0-M4'),
	@Grab(group='org.slf4j', module='slf4j-simple', version='1.7.5'),
	@Grab(group='org.apache.commons', module='commons-lang3', version='3.1')
	])


import com.datastax.driver.core.Cluster
import com.datastax.driver.core.Host
import com.datastax.driver.core.Metadata
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.Session
import com.datastax.driver.core.Row
import com.datastax.driver.core.KeyspaceMetadata
import com.datastax.driver.core.TableMetadata
import com.datastax.driver.core.ColumnMetadata
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.text.WordUtils
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

logger = LoggerFactory.getLogger("EZCassandraHelper")



//GLOBALS
classText = null
className = null
dataTypeMap = [ascii:'String', bigint:'Integer', "'org.apache.cassandra.db.marshal.BytesType'":'File', boolean:'boolean', 
	counter:'Integer', decimal:'Double', double:'Double', float:'Double', inet:'String', int:'int', text:'String', 
	timestamp:'Date', uuid:'UUID', timeuuid:'UUID', varchar:'String', varint:'Integer']

//Connection
Cluster cluster
Session session
Metadata metadata
List<KeyspaceMetadata> keyspaceMetadata
String host = 'localhost'
String keyspace = null

//RUNTIME OPTIONS
cli = new CliBuilder(usage:'DbInfo')
cli._(longOpt:'host', args:1, argName:'host','host to connect to, defaults to: localhost')
cli._(longOpt:'keyspace', args:1, argName:'keyspace', 'keyspace')
cli.o('Write out java bean stub class.')
cli._(longOpt:'dir', args:1, argName:'absolute_directory', 'The directory to write classes out to.  Used with -o option.')
cli._(longOpt:'pkg', args:1, argName:'java_package', 'The package to use with the generated classes.  Used with -o option.')

if(args.length == 0){
	cli.usage()	
	System.exit(0)
}

options = cli.parse(args)

if(options['host']){
	host = options.host
}
if(options['keyspace']){
	keyspace = options.keyspace
}

if(options.o){
	try{
		assert options.pkg && options.dir
	} catch (Error e){
		println "ERROR: --pkg and --dir options are required with the -o option."
		System.exit(-1)
	}
}


//METHODS
String classBegin(String tableName){
	tmpClassName = StringUtils.replace(tableName, "_", " ")
	tmpClassName = WordUtils.capitalizeFully(tmpClassName)
	tmpClassName = StringUtils.replace(tmpClassName, " ", "")
	className = tmpClassName

	begin = """
import java.util.UUID;
import java.lang.Integer;
import java.lang.Double;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.File;

@Entity(name = "${tableName}")
public class ${className} {
"""

	return "package ${options.pkg}\n\n"+begin

}

String createField(String columnName, String columnType){

	tmpFieldName = StringUtils.replace(columnName, "_", " ")
	tmpFieldName = WordUtils.capitalizeFully(tmpFieldName)
	tmpFieldName = StringUtils.replace(tmpFieldName, " ", "")
	tmpFieldName = WordUtils.uncapitalize(tmpFieldName)

	javaType = dataTypeMap.get(columnType)

	if(javaType == null){
		if(columnType.contains("map<")){
			javaType = parseMap(columnType)
		}else if (columnType.contains("list<")) {
			javaType = parseList(columnType)
		}else if (columnType.contains("set<")){
			javaType = parseSet(columnType)
		}else{
			println "ERROR - can not determine type: ${columnType}"
			System.exit(-1)
		}
	}


	return """\t@Column(name = "${columnName}")\n\tprivate ${javaType} ${tmpFieldName};\n\n"""
}

String parseMap(columnType){
	tmpType = StringUtils.removeStart(columnType, "map<")
	tmpType = StringUtils.removeEnd(tmpType,">")
	tmpType = StringUtils.replace(tmpType, " ", "")
	tmpArray = StringUtils.split(tmpType, ",")
	key = dataTypeMap.get(tmpArray[0])
	value = dataTypeMap.get(tmpArray[1])
	return "Map<${key}, ${value}>"
}

String parseList(columnType){
	tmpType = StringUtils.removeStart(columnType, "list<")
	tmpType = StringUtils.removeEnd(tmpType,">")
	tmpType = StringUtils.replace(tmpType, " ", "")
	type = dataTypeMap.get(tmpType)
	return "List<${type}>"

}

String parseSet(columnType){
	tmpType = StringUtils.removeStart(columnType, "set<")
	tmpType = StringUtils.removeEnd(tmpType,">")
	tmpType = StringUtils.replace(tmpType, " ", "")
	type = dataTypeMap.get(tmpType)
	return "Set<${type}>"

}

void writeFile(){
	fileName = options.dir + File.separator + className + ".java"
	def f = new File(fileName)
	f << classText
	println "${padTabs(1)}Writing file: ${fileName}"
	println ""

}

String padTabs(num){
	def range = 1..num
	def tab = "\t"
	def tabs = ""
	range.each{t ->
		tabs = tabs + tab
	}

	return tabs
}


//STARTING
println "Connecting to host: $host. Using keyspace: $keyspace"
println "FYI: system keyspaces are: system and system_traces"

cluster = Cluster.builder().addContactPoint(host).build().init();

metadata = cluster.getMetadata();

println "Connected to cluster: ${metadata.getClusterName()}"

for ( Host systemHost : metadata.getAllHosts() ) {
	println "Datatacenter: ${systemHost.getDatacenter()}; Host: ${systemHost.getAddress()}; Rack: ${systemHost.getRack()}"
}

keyspaceMetadata = metadata.getKeyspaces()

keyspaceMetadata.each{ ksmd -> 
	keyspaceName = ksmd.getName()
	if(keyspaceName.equals(keyspace)){
		println "KEYSPACE: $keyspaceName"	
		Collection<TableMetadata> tables = ksmd.getTables()

		tables.each{ table ->
			def tableName = table.getName()
			println "${padTabs(1)}TABLE: ${tableName}"
			classText = classBegin(tableName)

			println "${padTabs(2)}COLUMNS:"

			List<ColumnMetadata> columns = table.getColumns()
			
			columns.each{ column ->
				classText = classText + createField(column.getName(), column.getType().toString())
				println "${padTabs(3)}${column.getName()} : ${column.getType().toString()}"
			}
			

			classText = classText + "}"

			if(options.o){
				writeFile()
			}

		}



	}
	

}


cluster.shutdown()





